import logging
import math
import random
from datetime import datetime

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# RGB bytes
DEFAULT_COLORS = (
    b"\xf6\x51\x1d",
    b"\xff\xb4\x00",
    b"\x00\xa6\xed",
    b"\x7f\xb8\x00",
    b"\x0d\x2c\x54"
)


class Fixture:
    def __init__(self, decay: float):
        self._intensity = 0.0

        self.last_beat = datetime.now()
        self.decay = decay

    def update(self):
        delta_t = datetime.now() - self.last_beat
        t = float(delta_t.microseconds) / 1000000
        self._intensity *= math.exp(-self.decay * t)

    def beat(self):
        self._intensity = 1.0
        self.last_beat = datetime.now()

    @property
    def intensity(self):
        return self._intensity


class RGBFixture(Fixture):
    def __init__(self, decay: float, colors: list=None):
        super().__init__(decay)

        self._colors = colors or DEFAULT_COLORS
        self._color = random.choice(self._colors)

    def set_colors(self, colors):
        self._colors = colors

    def beat(self):
        super().beat()
        self._color = random.choice(self._colors)

    @property
    def color(self):
        return self._color


class Lightshow:
    def __init__(self):
        self.fixtures = []

    def register_fixture(self, fixture):
        self.fixtures.append(fixture)

    def update(self):
        for fixture in self.fixtures:
            fixture.update()

    def beat(self):
        for fixture in self.fixtures:
            fixture.beat()








