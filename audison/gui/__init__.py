import multiprocessing as mp
from multiprocessing.connection import Connection

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject

from .window import AudisonWindow  # noqa

GObject.threads_init()


class GUIProcess(mp.Process):
    def __init__(self, conn: Connection, audio_frames: mp.Array,
                 filename: str):
        super().__init__(name="GUI Process")
        self.conn = conn
        self.audio_frames = audio_frames
        self.filename = filename

    def run(self):
        window = AudisonWindow(self.conn, self.audio_frames, self.filename)
        window.connect('delete-event', Gtk.main_quit)
        window.show_all()

        Gtk.main()
