import math

from gi.repository import Gtk, Gdk
import cairo

from audison.lightshow import RGBFixture


class RGBLight(Gtk.DrawingArea, RGBFixture):
    """
    This class represents a single RGB light, with four parameters:

    - Master intensity
    - Red channel
    - Green channel
    - Blue channel

    The master intensity is modelled as the size of a circle.
    """

    def __init__(self, decay=0.008, *args, **kwargs):
        super().__init__(*args, **kwargs)
        RGBFixture.__init__(self, decay)

        # Cairo drawing surface
        self.surface = None

        self.connect('draw', self.on_draw)

        # Expand widget by default
        self.set_hexpand(True)
        self.set_vexpand(True)

    def on_draw(self, widget, ctx: cairo.Context):
        width = widget.get_allocated_width()
        height = widget.get_allocated_height()

        ctx.save()
        ctx.scale(width, height)
        ctx.translate(0.5, 0.5)
        ctx.arc(0, 0, 0.45, 0.0, 2 * math.pi)
        ctx.set_source_rgba(self.color[0]/255, self.color[1]/255,
                           self.color[2]/255, self.intensity)
        ctx.fill_preserve()
        ctx.restore()

    def update(self):
        super().update()

        rect = Gdk.Rectangle()
        rect.x = 0
        rect.y = 0
        rect.width = self.get_allocation().width
        rect.height = self.get_allocation().height

        window = self.get_window()
        if window:
            window.invalidate_rect(rect, False)

