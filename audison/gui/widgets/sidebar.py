from abc import ABCMeta, abstractproperty
from gi.repository import Gtk


class AudioRow(Gtk.ListBoxRow, metaclass=ABCMeta):
    def __init__(self):
        super().__init__()

        artist = Gtk.Label()
        artist.set_markup("<b>{}</b>".format(self.artist))
        self.add(artist)
        self.add(Gtk.Label(self.title))

    @abstractproperty
    def artist(self):
        pass

    @abstractproperty
    def title(self):
        pass
