import os
import logging
import multiprocessing as mp
from multiprocessing.connection import Connection

from gi.repository import Gtk, Gdk, GLib, GObject, Gst

from audison.lightshow import Lightshow
from audison.gui.widgets.light import RGBLight


logger = mp.get_logger()
logger.setLevel(logging.DEBUG)

Gst.init()


class AudisonWindow(Gtk.Window):
    def __init__(self, conn: Connection, audio_frames: mp.Array,
                 filename: str):
        super().__init__(title="Audison Visualisation")
        self.set_default_size(1024, 1024)

        # Interprocess communication and data
        self.conn = conn
        self.audio_frames = audio_frames
        self.beats = []
        self.filename = filename

        self.hb = Gtk.HeaderBar()
        self.hb.set_title("Audison")
        self.hb.set_subtitle("Analyzing beats...")
        self.hb.props.show_close_button = True
        self.set_titlebar(self.hb)

        self.init_style()

        # Add IO watch for connection with playback process
        GLib.io_add_watch(self.conn.fileno(), GLib.IO_IN, self.on_command)

        # Main View
        # --------------------------------

        self.lights = []

        vbox = Gtk.VBox()
        vbox.get_style_context().add_class('lamps-container')

        # -- Lights grid
        self.lightshow = Lightshow()
        self.lights_grid = Gtk.Grid()
        self.lights_grid.set_row_spacing(5)
        self.lights_grid.set_column_spacing(5)
        vbox.pack_start(self.lights_grid, True, True, 0)

        for x in range(3):
            self.lights.append([])
            for y in range(3):
                light = RGBLight()
                self.lightshow.register_fixture(light)
                self.lights[x].append(light)

                self.lights_grid.attach(light, x, y, 1, 1)

        # Add both panes to the main window
        self.add(vbox)
        self.show_all()

        self.conn.send(("analyze", {}))
        self.connect('delete-event', self.on_quit)

        # Gstreamer pipeline
        # --------------------------------------
        self.playbin = Gst.ElementFactory.make("playbin", "playbin")
        self.playbin.set_property("uri", "file://" +
                                  os.path.abspath(self.filename))

    def init_style(self):
        style_provider = Gtk.CssProvider()
        style_provider.load_from_data(b"""
        .lamps-container {
            background-color: black;
        }
        """)

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    def update(self, *args, **kwargs):
        success, position = self.playbin.query_position(Gst.Format.TIME)
        if not success:
            logger.error("Could not fetch playback position")
            return True

        seconds = float(position) / Gst.SECOND

        if not self.beats:
            # No more beats available, notify GLib to stop calling this
            # function
            logger.debug("Reached last beat")
            return False

        if seconds >= self.beats[0]:
            print("beat")
            print(seconds, self.beats[0])
            self.beats.pop(0)
            self.lightshow.beat()

        self.lightshow.update()

        return True  # Let GLib know to reschedule this function

    def on_command(self, fd, cond):
        logger.debug("Receiving command...")
        command, args = self.conn.recv()

        logger.debug("Received command: %s, args %s", command, args)

        funcname = 'on_' + command
        if hasattr(self, funcname):
            func = getattr(self, funcname)
            func(args)

    def on_beats_detected(self, args):
        logger.debug("Received beats: %s", args['beats'])
        self.beats = args['beats']

        # Start the function that continuously checks if there is a beat,
        # and updates the lights on each call
        GLib.idle_add(self.update)

        self.playbin.set_state(Gst.State.PLAYING)
        self.hb.set_subtitle("Playing {}".format(self.filename))

    def on_quit(self, *args, **kwargs):
        self.conn.send(("stop", {}))

