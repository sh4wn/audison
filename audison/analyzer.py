"""
Record audio from the audio device, allowing for realtime light show design
based on the currently playing audio.
"""
import logging
import multiprocessing as mp
from multiprocessing.connection import Connection
from typing import Tuple

import numpy as np
from madmom.features.beats import RNNBeatProcessor, BeatDetectionProcessor
from gi.repository import GObject

logger = mp.get_logger()
logger.setLevel(logging.DEBUG)
GObject.threads_init()


class AnalyzerProcess(mp.Process):
    def __init__(self, connection: Connection, audio_frames: mp.Array):
        super().__init__(name=self.__class__.__name__)

        self.conn = connection
        self.audio_frames = audio_frames

    def run(self):
        while True:
            command, args = self.conn.recv()  # type: Tuple[str, dict]
            logger.debug("Received command: %s, args: %s", command, args)

            if command == "stop":
                break

            if command == "analyze":
                logger.info("Analyzing beats")

                in_processor = RNNBeatProcessor(min_bpm=80)
                out_processor = BeatDetectionProcessor(min_bpm=80, fps=100)

                audio_np = np.frombuffer(self.audio_frames.get_obj(),
                                         dtype='f4')
                activations = in_processor(audio_np)
                beats = out_processor(activations).tolist()

                self.conn.send(("beats_detected", {"beats": beats}))

                break

