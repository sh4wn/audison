#!/usr/bin/env python3
import os
import logging
import argparse
import multiprocessing as mp

import gi
import matplotlib
from audioread.maddec import MadAudioFile
import numpy as np

gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
matplotlib.use('gtk3cairo')

from gi.repository import GObject
from librosa.core import to_mono, resample
from librosa import util

GObject.threads_init()

logger = mp.log_to_stderr()
logger.setLevel(logging.DEBUG)


def load_audio(path, sr=44100, mono=True, offset=0.0, duration=None,
         dtype=np.float32):
    """Load an audio file as a floating point time series.

    Parameters
    ----------
    path : string
        path to the input file.

        Any format supported by `audioread` will work.

    sr   : number > 0 [scalar]
        target sampling rate

        'None' uses the native sampling rate

    mono : bool
        convert signal to mono

    offset : float
        start reading after this time (in seconds)

    duration : float
        only load up to this much audio (in seconds)

    dtype : numeric type
        data type of `y`


    Returns
    -------
    y    : np.ndarray [shape=(n,) or (2, n)]
        audio time series

    sr   : number > 0 [scalar]
        sampling rate of `y`


    Examples
    --------
    >>> # Load a wav file
    >>> filename = librosa.util.example_audio_file()
    >>> y, sr = librosa.load(filename)
    >>> y
    array([ -4.756e-06,  -6.020e-06, ...,  -1.040e-06,   0.000e+00], dtype=float32)
    >>> sr
    22050

    >>> # Load a wav file and resample to 11 KHz
    >>> filename = librosa.util.example_audio_file()
    >>> y, sr = librosa.load(filename, sr=11025)
    >>> y
    array([ -2.077e-06,  -2.928e-06, ...,  -4.395e-06,   0.000e+00], dtype=float32)
    >>> sr
    11025

    >>> # Load 5 seconds of a wav file, starting 15 seconds in
    >>> filename = librosa.util.example_audio_file()
    >>> y, sr = librosa.load(filename, offset=15.0, duration=5.0)
    >>> y
    array([ 0.069,  0.1  , ..., -0.101,  0.   ], dtype=float32)
    >>> sr
    22050

    """

    y = []
    with MadAudioFile(os.path.realpath(path)) as input_file:
        sr_native = input_file.samplerate
        n_channels = input_file.channels

        s_start = int(np.round(sr_native * offset)) * n_channels

        if duration is None:
            s_end = np.inf
        else:
            s_end = s_start + (int(np.round(sr_native * duration))
                               * n_channels)

        n = 0

        for frame in input_file:
            frame = util.buf_to_float(frame, dtype=dtype)
            n_prev = n
            n += len(frame)

            if n < s_start:
                # offset is after the current frame
                # keep reading
                continue

            if s_end < n_prev:
                # we're off the end.  stop reading
                break

            if s_end < n:
                # the end is in this frame.  crop.
                frame = frame[:s_end - n_prev]

            if n_prev <= s_start <= n:
                # beginning is in this frame
                frame = frame[(s_start - n_prev):]

            # tack on the current frame
            y.append(frame)

    if y:
        y = np.concatenate(y)

        if n_channels > 1:
            y = y.reshape((-1, 2)).T
            if mono:
                y = to_mono(y)

        if sr is not None:
            y = resample(y, sr_native, sr)
        else:
            sr = sr_native

    # Final cleanup for dtype and contiguity
    y = np.ascontiguousarray(y, dtype=dtype)

    return y


def main(args):
    from audison.analyzer import AnalyzerProcess
    from audison.gui import GUIProcess

    parent_conn, child_conn = mp.Pipe()

    logger.info("Loading audio file %s", args.file)
    audio = load_audio(args.file)
    shared_audio = mp.Array('f', len(audio))
    shared_np = np.frombuffer(shared_audio.get_obj(), dtype='f4')
    shared_np[:] = audio
    del audio

    gui = GUIProcess(parent_conn, shared_audio, args.file)
    player = AnalyzerProcess(child_conn, shared_audio)

    player.start()
    gui.start()

    player.join()
    gui.join()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Automatic Light Show Generation"
    )

    parser.add_argument('file', help="The audio file to analyze and play")
    args = parser.parse_args()

    main(args)
